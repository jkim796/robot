#include <Servo.h>

#define START_POS 90
#define LEFT 180
#define RIGHT 0
#define OBJECT_DIST 25
#define SWEEP_DEG 10
#define DELAY_PARAM 1000

int speedPin_M1 = 5;
int speedPin_M2 = 6;
int directionPin_M1 = 4;
int directionPin_M2 = 7;
unsigned long actualDistance = 0;

Servo myservo;
int pos = START_POS;
int sweepFlag = 1;

int URPWM = 3;
int URTRIG = 10;
uint8_t EnPwmCmd[4] = {0x44, 0x02, 0xbb, 0x01};

void setup()
{
  Serial.begin(9600);
  myservo.attach(9);
  myservo.write(START_POS);
  pinMode(URTRIG, OUTPUT);
  pinMode(URPWM, INPUT);
  digitalWrite(URTRIG, HIGH);
  for (int i = 0; i < 4; i++)
  {
    Serial.write(EnPwmCmd[i]);
  }
}

void loop()
{
  carAdvance(255, 255);
  actualDistance = measureDistance(5);
  Serial.println(actualDistance);
  if (actualDistance <= OBJECT_DIST)
  {
    carStop();
    myservo.write(0);
    delay(1000);
    pos = 0;
    do
    {
      servoSweep();
      Serial.print("sweeping: ");
      Serial.println(pos);
      actualDistance = measureDistance(5);
      Serial.println(actualDistance);
      if (actualDistance > OBJECT_DIST)
      {
        Serial.println("breaking");
        break;
      }
    } while (pos < 180);
    if (pos == 180)
    {
      myservo.write(90);
      delay(1000);
      pos = 90;
      carBack(255, 255);
      delay(1000);
      carTurnRight(255,255);
      delay(3500);
    }
    else
    {
      if (pos <= 90)
      {
        int turnDelay = 2550 - ((pos * 28) + 30);
        myservo.write(90);
        delay(1000);
        pos = 90;
        carTurnRight(255,255);
        delay(turnDelay);
      }
      else
      {
        int turnDelay = ((pos - 90) * 28) + 30;
        myservo.write(90);
        delay(1000);
        pos = 90;
        carTurnLeft(255,255);
        delay(turnDelay);
      }
    }
  }
}

int measureDistance(int num_reads)
{
  if (num_reads <= 0) return 0;
  int sum = 0;
  unsigned long distance = 0;
  for (int i = 0; i < num_reads; i++)
  {
    digitalWrite(URTRIG, LOW);
    delayMicroseconds(2);
    digitalWrite(URTRIG, HIGH);
    delayMicroseconds(10);
    distance = pulseIn(URPWM, LOW) / 50;
    sum = sum + distance;
    delay(25);
  }
  return sum / num_reads;
}

void carStop() 
{
  digitalWrite(speedPin_M2, 0);
  digitalWrite(directionPin_M1, LOW);
  digitalWrite(speedPin_M1, 0);
  digitalWrite(directionPin_M2, LOW);
}

void carBack(int leftSpeed, int rightSpeed)
{
  analogWrite(speedPin_M2, leftSpeed);
  digitalWrite(directionPin_M1, LOW);             
  analogWrite(speedPin_M1, rightSpeed);
  digitalWrite(directionPin_M2, LOW);
}

void carAdvance(int leftSpeed, int rightSpeed)
{
  analogWrite(speedPin_M2, leftSpeed);
  digitalWrite(directionPin_M1, HIGH);
  analogWrite(speedPin_M1, rightSpeed);
  digitalWrite(directionPin_M2, HIGH);
}

void carTurnLeft(int leftSpeed, int rightSpeed)
{
  analogWrite(speedPin_M2, leftSpeed);
  digitalWrite(directionPin_M1, HIGH);
  analogWrite(speedPin_M1, rightSpeed);
  digitalWrite(directionPin_M2, LOW);
}
void carTurnRight(int leftSpeed, int rightSpeed)
{
  analogWrite(speedPin_M2, leftSpeed);
  digitalWrite(directionPin_M1, LOW);
  analogWrite(speedPin_M1, rightSpeed);
  digitalWrite(directionPin_M2, HIGH);
}

void servoSweep()
{
  if (sweepFlag)
  {
    if (pos >= RIGHT && pos <= LEFT)
    {
      pos += SWEEP_DEG;
      myservo.write(pos);
    }
    if (pos > LEFT - SWEEP_DEG) sweepFlag = false;
  }
  else
  {
    if (pos >= RIGHT && pos <= LEFT)
    {
      pos -= SWEEP_DEG;
      myservo.write(pos);
    }
    if (pos < RIGHT + SWEEP_DEG) sweepFlag = true;
  }
}